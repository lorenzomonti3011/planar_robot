#include "control.h"
#include <iostream>
Controller::Controller(RobotModel &rmIn):
  model  (rmIn)
{
}

Eigen::Vector2d Controller::Dqd ( 
          const Eigen::Vector2d & q,
          const Eigen::Vector2d & xd,
          const Eigen::Vector2d & Dxd_ff
                      )
{  
  //TODO Compute joint velocities able to track the desired cartesian position
  Eigen::Vector2d Dqd;

  model.FwdKin(X, J, q);

  dX_desired = kp*(xd-X);
  Dqd = J.inverse()*(Dxd_ff + dX_desired);
  //Dqd = J.inverse()*(dX_desired);

  return Dqd;
}

