#include "kinematic_model.h"
#include <iostream>
#include <cmath> 

using namespace std;

const double l1 =  2;
const double l2 =  2;
RobotModel myRobotModel(l1,l2);

int main(){
  // Compute the forward kinematics and jacobian matrix for 
  //      q =  M_PI/3.0,  M_PI_4
  // For a small variation of q, compute the variation on X and check dx = J . dq  

  Eigen::Vector2d q = Eigen::Vector2d::Zero();
  Eigen::Vector2d x1 = Eigen::Vector2d::Zero();
  Eigen::Vector2d x2 = Eigen::Vector2d::Zero();
  Eigen::Matrix2d J;

  q(0) = M_PI/3;
  q(1) = M_PI/4;

  myRobotModel.FwdKin(x1, J, q);

  cout << "x = " << x1 << "\n";
  cout << "J = " << J << "\n\n";

  q(0) = (M_PI/3)+0.05;
  q(1) = (M_PI/4)+0.05;

  Eigen::Vector2d dq;
  Eigen::Vector2d dx;

  dq(0)=0.05;
  dq(1)=0.05;
  //dx = J*dq;

  myRobotModel.FwdKin(x2, J, q);

  dx = J*dq;
  
  cout << "dx = " << dx << "\n";
  cout << "x2-x1 = " << x2-x1 << "\n";

}
