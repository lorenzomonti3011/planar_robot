#include "control.h"
#include "trajectory_generation.h"
#include <iostream>

using namespace std;


int main(){
  // Show using all three libraries, simulate the motion of a planar robot
  // For a initial position 
  //      q   = M_PI_2, M_PI_4
  // and  
  //      l1  = 0.4, l2 = 0.5
  // simulate a motion achieving 
  //      Xf  = 0.0, 0.6

  const double l1  = 0.4;
  const double l2 = 0.5; 
  const double Dt =  5;

  Eigen::Vector2d q(M_PI_2,M_PI_4);
  Eigen::Vector2d Xf(0,0.6);
  Eigen::Vector2d Xi;
  Eigen::Vector2d xd;
  Eigen::Matrix2d J;
  Eigen::Vector2d Dxd_ff;
  Eigen::Vector2d Dqd;


  RobotModel myRobotModel(l1,l2);
  Controller myController(myRobotModel);

  myRobotModel.FwdKin(Xi, J, q);
  Point2Point myPoint2Point(Xi, Xf, Dt);

  //cout << "Xi" << Xi <<"\n";

 

  double t = 0;
  int cpt = 0;
  const double dt = 1e-3;
  Eigen::Vector2d Xactual = Eigen::Vector2d::Zero();
  //Eigen::Matrix2d J = Eigen::Matrix2d::Zero();
  while(t < Dt)
  {

    xd = myPoint2Point.X(t);
    Dxd_ff = myPoint2Point.dX(t);

    Dqd =  myController.Dqd(q, xd, Dxd_ff);
   // cout << "Dqd = " << Dqd <<"\n\n";

    q = (Dqd*dt) + q;

    myRobotModel.FwdKin(Xactual,J,q);

    if(cpt%499 == 0){
      cout << "t = " << t <<"\n";
      cout << "xd = " << xd <<"\n";
      cout << "Xactual = " << Xactual <<"\n\n";

    }
    
    cpt = cpt+1;
    t = t+dt;

  }

}