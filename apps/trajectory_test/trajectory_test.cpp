#include "trajectory_generation.h"
#include <iostream>

using namespace std;

const double Dt =  5;
const double dt =  pow(10,-6);

Eigen::Vector2d X_i (0,0);
Eigen::Vector2d X_f (1,2);

Point2Point myPoint2Point(X_i, X_f, Dt);

int main(){

  // Compute the trajectory for given initial and final positions. 
  // Display some of the computed points
  // Check numerically the velocities given by the library 
  // Check initial and final velocities

  Eigen::Vector2d Xa;
  Eigen::Vector2d dXa;
  Eigen::Vector2d dXn;

  float t = 0;

  while(t < Dt)
  {
    Xa = myPoint2Point.X(t);
    dXa = myPoint2Point.dX(t);

    dXn = (myPoint2Point.X(t+dt)-myPoint2Point.X(t))/dt;

    cout << "Xa = " << Xa << "\n\n\n";
    cout << "dXa = " << dXa << "\ndXn = " << dXn << "\n\n\n";

    t = t+0.1;

  }



  
   
}